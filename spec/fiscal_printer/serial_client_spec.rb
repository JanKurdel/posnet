require 'spec_helper'
require 'serial_client'

module FiscalPrinter
  RSpec.describe SerialClient do
    let(:client) { SerialClient.new({ port: '/dev/tty.usbmodem1421' }) }

    it 'should process response when correct message is sent' do
      message = Thermal::Message::Getprinterinfo.new(command: 23)
      client.write(message)
      sleep(5)
    end

    it 'should raise an error when wrong message is sent' do
    end
  end
end
