require 'spec_helper'

module FiscalPrinter
  module Thermal
    module Message
      RSpec.describe Rtcget do
        it 'Returns correct message' do
          message = Rtcget.new

          expect(message.to_s).to eq "#{ESC}P0#c8F#{ESC}\\".force_encoding('BINARY')
          expect(message.valid?).to be true
          expect(message.errors).to be_empty
        end
      end
    end
  end
end
