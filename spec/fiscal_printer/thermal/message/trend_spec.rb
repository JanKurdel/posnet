require 'spec_helper'

module FiscalPrinter
  module Thermal
    module Message
      RSpec.describe Trend do
        it 'Returns correct message' do
          message = Trend.new(
            payment_value: 100,
            total_value: 20
          )

          expect(message.to_s).to eq "#{ESC}P1;0$e1ab#{CR}100.0/20.0/88#{ESC}\\".force_encoding('BINARY')
          expect(message.valid?).to be true
          expect(message.errors).to be_empty
        end
      end
    end
  end
end
