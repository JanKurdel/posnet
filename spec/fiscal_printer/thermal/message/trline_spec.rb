require 'spec_helper'

module FiscalPrinter
  module Thermal
    module Message
      RSpec.describe Trline do
        let(:message_params) do
          {
            line_number: 1,
            name: 'Bun z wieprzowina',
            quantity: 1,
            ptu: 'A',
            price: 14,
            total_price: 14
          }
        end

        it 'Returns correct message' do
          message = Trline.new(message_params)
          expect(message.to_s).to eq "#{ESC}P1$lBun z wieprzowina#{CR}1#{CR}A/14.0/14.0/87#{ESC}\\".force_encoding('BINARY')
          expect(message.valid?).to be true
          expect(message.errors).to be_empty
        end

        it 'Does not allow print in line greater than 255' do
          message = Trline.new(message_params.merge(line_number: 256))
          expect(message.valid?).to_not be true
          expect(message.errors).to have_key(:line_number)
        end

        it 'Requires line_number, name, ptu, price, total_price to be filled in' do
          message = Trline.new({})
          expect(message.valid?).to_not be true
          expect(message.errors).to have_key(:line_number)
          expect(message.errors).to have_key(:name)
          expect(message.errors).to have_key(:ptu)
          expect(message.errors).to have_key(:price)
          expect(message.errors).to have_key(:total_price)
        end

        it 'Returns message with discount' do
          message = Trline.new(
            message_params.merge(
              discount_type: 1,
              discount: 10
            )
          )
          expect(message.to_s).to eq "#{ESC}P1;1$lBun z wieprzowina#{CR}1#{CR}A/14.0/14.0/8D#{ESC}\\".force_encoding('BINARY')
          expect(message.valid?).to be true
          expect(message.errors).to be_empty
        end

        it 'Returns message with discount with custom description' do
          message = Trline.new(
            message_params.merge(
              discount_type: 1,
              discount: 10,
              discount_description: 16,
              custom_discount_description: 'Rabat Minha'
            )
          )
          expect(message.to_s).to eq "#{ESC}P1;1;16$lBun z wieprzowina#{CR}1#{CR}A/14.0/14.0/Rabat Minha#{CR}9B#{ESC}\\".force_encoding('BINARY')
          expect(message.valid?).to be true
          expect(message.errors).to be_empty
        end

        it 'Returns message with discount, custom discount and item description' do
          message = Trline.new(
            message_params.merge(
              discount_type: 1,
              discount: 10,
              discount_description: 16,
              custom_discount_description: 'Rabat Minha',
              description: 'Zrobione przez Minha'
            )
          )
          expect(message.to_s).to eq "#{ESC}P1;1;16;1$lBun z wieprzowina#{CR}1#{CR}A/14.0/14.0/Zrobione przez Minha#{CR}Rabat Minha#{CR}90#{ESC}\\".force_encoding('BINARY')
          expect(message.valid?).to be true
          expect(message.errors).to be_empty
        end

        it 'Returns message with discount and item custom description' do
          message = Trline.new(
            message_params.merge(
              discount_type: 1,
              discount: 10,
              discount_description: 9,
              description: 'Zrobione przez Minha'
            )
          )
          expect(message.to_s).to eq "#{ESC}P1;1;9;1$lBun z wieprzowina#{CR}1#{CR}A/14.0/14.0/Zrobione przez Minha#{CR}84#{ESC}\\".force_encoding('BINARY')
          expect(message.valid?).to be true
          expect(message.errors).to be_empty
        end

        it 'Returns message with custom item description' do
          message = Trline.new(
            message_params.merge(
              discount_type: 0,
              discount_description: 0,
              description: 'Zrobione przez Minha'
            )
          )
          expect(message.to_s).to eq "#{ESC}P1;0;0;1$lBun z wieprzowina#{CR}1#{CR}A/14.0/14.0/Zrobione przez Minha#{CR}8C#{ESC}\\".force_encoding('BINARY')
          expect(message.valid?).to be true
          expect(message.errors).to be_empty
        end
      end
    end
  end
end
