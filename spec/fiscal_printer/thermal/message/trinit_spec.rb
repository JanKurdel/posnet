require 'spec_helper'

module FiscalPrinter
  module Thermal
    module Message
      RSpec.describe Trinit do
        it 'Returns correct message' do
          message = Trinit.new(items_count: 20)

          expect(message.to_s).to eq "#{ESC}P20$hB1#{ESC}\\".force_encoding('BINARY')
          expect(message.valid?).to be true
          expect(message.errors).to be_empty
        end

        it 'Does not allow transaction with more than 80 lines' do
          message = Trinit.new(items_count: 81)

          expect(message.valid?).to be false
          expect(message.errors).to_not be_empty
        end

        it 'Does not allow transaction with less than 0 lines' do
          message = Trinit.new(items_count: -1)

          expect(message.valid?).to be false
          expect(message.errors).to_not be_empty
        end
      end
    end
  end
end
