require 'spec_helper'
require 'fiscal_printer/serial_client'

module FiscalPrinter
  module Posnet
    RSpec.describe Driver do

      before(:all) do
        serial = SerialClient.new({ port: '/dev/tty.usbmodem1' })
        @driver = Driver.new(serial)
      end

      describe '#print_receipt', fails: true do
        it 'Prints receipt when correct receipt is passed' do
          lines = [
            Line.new(description: 'Item 1', count: 1.0, price: 10.0, total_price: 10.0, ptu: 'B'),
            Line.new(description: 'Item 2', count: 1.0, price: 20.0, total_price: 20.0, ptu: 'A'),
            Line.new(description: 'Item 3', count: 2.0, price: 30.0, total_price: 60.0, ptu: 'A'),
          ]
          receipt = Receipt.new(
            receipt_id: 10,
            payments_total: 190,
            price_total: 90,
            lines: lines
          )

          @driver.print_receipt(receipt)
        end
      end

      describe '#set_header' do
        it 'sets header if correct header is passed' do
          @driver.set_header("<center>THE BEST SYSTEM FOR RESTAURANTS</center>\n<center><b>INORDER</b></center>\n")
        end
      end

      describe '#get_header' do
        it 'gets header if correct message is passed' do
          @driver.get_header
        end
      end

      describe '#set_ptu' do
        it 'sets ptu values' do
          @driver.set_ptu({ A: 23, B: 8, C: 0, D: 5, E: 101, F: 101, G: 100})
        end
      end

      describe '#get_ptu' do
        it 'gets ptu values' do
          @driver.get_ptu
        end
      end
    end
  end
end
