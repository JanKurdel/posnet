require 'spec_helper'
require 'posnet/message/opendrwr'

module Posnet
  module Message
    RSpec.describe Opendrwr do
      it 'Returns correct message' do
        message = Opendrwr.new
        expect(message.to_s).to eq "#{STX}opendrwr#{TAB}#\xc1\x18#{ETX}".force_encoding('BINARY')
        expect(message.valid?).to be true
        expect(message.errors).to be_empty
      end
    end
  end
end
