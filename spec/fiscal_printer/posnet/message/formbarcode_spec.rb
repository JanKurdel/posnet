require 'spec_helper'
require 'posnet/message/formbarcode'

module Posnet
  module Message
    RSpec.describe Formbarcode do
      it 'Returns correct message' do
        message = Formbarcode.new(
          fn: 2,
          bc: '987098912'
        )
        expect(message.to_s).to eq "#{STX}formbarcode#{TAB}fn2#{TAB}bc987098912#{TAB}#\b%#{ETX}".force_encoding('BINARY')
        expect(message.valid?).to be true
        expect(message.errors).to be_empty
      end

      it 'Returns errors for message without required fn and bc' do
        expect { Formbarcode.new }.to raise_error TypeError
      end
    end
  end
end
