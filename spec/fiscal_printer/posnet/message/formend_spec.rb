require 'spec_helper'
require 'posnet/message/formend'

module Posnet
  module Message
    RSpec.describe Formend do
      it 'Returns correct message' do
        message = Formend.new(
          fn: 2
        )
        expect(message.to_s).to eq "#{STX}formend#{TAB}fn2#{TAB}#-/#{ETX}".force_encoding('BINARY')
        expect(message.valid?).to be true
        expect(message.errors).to be_empty
      end

      it 'Returns errors for message without required fn and fl' do
        expect { Formend.new }.to raise_error TypeError
      end
    end
  end
end
