require 'spec_helper'
require 'posnet/message/formcmd'

module Posnet
  module Message
    RSpec.describe Formcmd do
      it 'Returns correct message' do
        message = Formcmd.new(
          fn: 2,
          cm: 1
        )
        expect(message.to_s).to eq "#{STX}formcmd#{TAB}fn2#{TAB}cm1#{TAB}#\xa2\xa5#{ETX}".force_encoding('BINARY')
        expect(message.valid?).to be true
        expect(message.errors).to be_empty
      end

      it 'Returns errors for message without required fn and cm' do
        expect { Formcmd.new }.to raise_error TypeError
      end
    end
  end
end
