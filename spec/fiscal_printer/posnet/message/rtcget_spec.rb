require 'spec_helper'

module FiscalPrinter
  module Posnet
    module Message
      RSpec.describe Rtcget do
        before(:all) do
          serial = SerialClient.new({ port: '/dev/tty.usbmodem1' })
          @driver = Driver.new(serial)
        end

        it 'Returns correct message' do
          message = Rtcget.new
          #@serial.write(message)
          @driver.get_rtc
          expect(message.to_s).to eq "#{STX}rtcget#{TAB}#\x42\x75#{ETX}"
          expect(message.valid?).to be true
          expect(message.errors).to be_empty
        end
      end
    end
  end
end
