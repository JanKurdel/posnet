require 'spec_helper'
require 'posnet/message/rtcset'

module Posnet
  module Message
    RSpec.describe Rtcset do
      it 'Returns correct message' do
        message = Rtcset.new(da: '2006-10-20;11:49')
        expect(message.to_s).to eq "#{STX}rtcset#{TAB}da2006-10-20;11:49#{TAB}#\xc2\x61#{ETX}".force_encoding('BINARY')
        expect(message.valid?).to be true
        expect(message.errors).to be_empty
      end

      it 'Returns errors for message without required datetime' do
        message = Rtcset.new
        expect(message.errors).to_not be_empty
      end

      it 'Returns errors for message with wrong formated datetime' do
        message = Rtcset.new(da: 'wrong date time format')
        expect(message.errors).to_not be_empty
      end
    end
  end
end
