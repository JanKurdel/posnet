require 'spec_helper'
require 'posnet/message/trline'

module Posnet
  module Message
    RSpec.describe Trline do
      it 'Returns correct message' do
        message = Trline.new(
          na: 'Mleko',
          vt: 2,
          pr: 245
        )
        expect(message.to_s).to eq "#{STX}trline#{TAB}naMleko#{TAB}vt2#{TAB}pr245#{TAB}##\xe7#{ETX}".force_encoding('BINARY')
        expect(message.valid?).to be true
        expect(message.errors).to be_empty
      end
    end
  end
end
