require 'spec_helper'

module Posnet
  module Message
    RSpec.describe Cashrep do
      it 'Returns correct message' do
        message = Cashrep.new
        expect(message.to_s).to eq "#{STX}cashrep#{TAB}#\x93\x8b#{ETX}".force_encoding('BINARY')
        expect(message.valid?).to be true
        expect(message.errors).to be_empty
      end
    end
  end
end
