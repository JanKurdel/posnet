require 'spec_helper'
require 'posnet/message/trinit'

module Posnet
  module Message
    RSpec.describe Trinit do
      it 'Returns correct message' do
        message = Trinit.new(
          bm: true
        )
        expect(message.to_s).to eq "#{STX}trinit#{TAB}bm1#{TAB}#Y\xf9#{ETX}".force_encoding('BINARY')
        expect(message.valid?).to be true
        expect(message.errors).to be_empty
      end

      it 'Returns errors for message with wrong bm value' do
        message = Trinit.new(bm: 'wrong boolean')
        expect(message.valid?).to be false
        expect(message.errors).to_not be_empty
      end
    end
  end
end
