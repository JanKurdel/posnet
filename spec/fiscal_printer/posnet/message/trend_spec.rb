require 'spec_helper'
require 'posnet/message/trend'

module Posnet
  module Message
    RSpec.describe Trend do
      it 'Returns correct message' do
        message = Trend.new(
          to: 200,
          re: 300,
          fp: 500
        )
        expect(message.to_s).to eq "#{STX}trend#{TAB}to200#{TAB}fp500#{TAB}re300#{TAB}#\x17B#{ETX}".force_encoding('BINARY')
        expect(message.valid?).to be true
        expect(message.errors).to be_empty
      end
    end
  end
end
