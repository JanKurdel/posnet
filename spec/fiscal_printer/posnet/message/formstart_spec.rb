require 'spec_helper'
require 'posnet/message/formstart'

module Posnet
  module Message
    RSpec.describe Formstart do
      it 'Returns correct message' do
        message = Formstart.new(
          fn: 12
        )
        expect(message.to_s).to eq "#{STX}formstart#{TAB}fn12#{TAB}#\xabJ#{ETX}".force_encoding('BINARY')
        expect(message.valid?).to be true
        expect(message.errors).to be_empty
      end

      it 'Returns errors for message without required fn' do
        expect { Formstart.new }.to raise_error TypeError
      end
    end
  end
end
