require 'spec_helper'
require 'posnet/message/trpayment'

module Posnet
  module Message
    RSpec.describe Trpayment do
      it 'Returns correct message' do
        message = Trpayment.new(
          ty: 2,
          wa: 500,
          re: 0
        )
        expect(message.to_s).to eq "#{STX}trpayment#{TAB}ty2#{TAB}wa500#{TAB}re0#{TAB}#\x1e\x96#{ETX}".force_encoding('BINARY')
        expect(message.valid?).to be true
        expect(message.errors).to be_empty
      end
    end
  end
end
