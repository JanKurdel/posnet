require 'spec_helper'
require 'posnet/message/vatset'

module Posnet
  module Message
    RSpec.describe Vatset do
      it 'Returns correct message' do
        message = Vatset.new(
          va: 123,
          vb: nil,
          vf: nil,
          vg: nil
        )
        expect(message.to_s).to eq "#{STX}vatset#{TAB}va123.0#{TAB}#\xDCV#{ETX}".force_encoding('BINARY')
        expect(message.valid?).to be true
        expect(message.errors).to be_empty
      end

      it 'Returns errors for message with wrong formated datetime' do
        message = Vatset.new(da: 'wrong date time format')
        expect(message.errors).to_not be_empty
      end
    end
  end
end
