require 'spec_helper'
require 'posnet/message/formline'

module Posnet
  module Message
    RSpec.describe Formline do
      it 'Returns correct message' do
        message = Formline.new(
          s1: "1234#{LF}BBBBB",
          fn: 3,
          fl: 10
        )
        expect(message.to_s).to eq "#{STX}formline#{TAB}s11234#{LF}BBBBB#{TAB}fn3#{TAB}fl10#{TAB}#R)#{ETX}".force_encoding('BINARY')
        expect(message.valid?).to be true
        expect(message.errors).to be_empty
      end

      it 'Returns errors for message without required fn and fl' do
        expect { Formline.new }.to raise_error TypeError
      end
    end
  end
end
