require 'spec_helper'
require 'posnet/message/dailyrep'

module Posnet
  module Message
    RSpec.describe Dailyrep do
      it 'Returns correct message' do
        message = Dailyrep.new(da: '2006-10-20')
        expect(message.to_s).to eq "#{STX}dailyrep#{TAB}da2006-10-20#{TAB}#\xca\xb0#{ETX}".force_encoding('BINARY')
        expect(message.valid?).to be true
        expect(message.errors).to be_empty
      end

      it 'Returns correct message without date specified' do
        message = Dailyrep.new
        expect(message.errors).to be_empty
      end

      it 'Returns errors for message with wrong formated date' do
        message = Dailyrep.new(da: 'wrong date time format')
        expect(message.errors).to_not be_empty
      end
    end
  end
end
