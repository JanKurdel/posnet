require 'spec_helper'
require 'posnet/message/hdrget'

module Posnet
  module Message
    RSpec.describe Hdrget do
      it 'Returns correct message' do
        message = Hdrget.new
        expect(message.to_s).to eq "#{STX}hdrget#{TAB}#\xb7\xba#{ETX}".force_encoding('BINARY')
        expect(message.valid?).to be true
        expect(message.errors).to be_empty
      end
    end
  end
end
