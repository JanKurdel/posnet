require 'artii'

fonts_list = Artii::CLI.new '--list'
fonts = fonts_list.output.split("\n")

fonts.each do |f|
  begin
    font = Artii::Base.new font: f
    lines = font.asciify('1 2 3 4 5 6 7 8 9 0').split("\n")
    lines.reject { |l| l.strip.empty? }
    if lines.count <= 6
      p "Front #{f}"
      lines.each { |l| p l }
    end
  rescue
  end
end
