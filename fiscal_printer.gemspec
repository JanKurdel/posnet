# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'fiscal_printer/version'

Gem::Specification.new do |spec|
  spec.name          = "fiscal_printer"
  spec.version       = FiscalPrinter::VERSION
  spec.authors       = ["Jan Kurdel"]
  spec.email         = ["jan.kurdel@gmail.com"]

  spec.summary       = %q{Ruby fiscal printers protocols implementation.}
  spec.description   = %q{Ruby Posnet/Thermal/Elzab protocols implementation.}
  spec.homepage      = "https://bitbucket.org/JanKurdel/fiscal_printer"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] = "TODO: Set to 'http://mygemserver.com'"
  else
    raise "RubyGems 2.0 or newer is required to protect against " \
      "public gem pushes."
  end

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_dependency "artii"
  spec.add_dependency "digest-crc"
  spec.add_dependency "dry-monads"
  spec.add_dependency "dry-struct"
  spec.add_dependency "dry-validation"
  spec.add_dependency "rubyserial"

  spec.add_development_dependency "bundler", "~> 1.14"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec", "~> 3.0"
  spec.add_development_dependency "rubocop"
  spec.add_development_dependency "pry"
end
