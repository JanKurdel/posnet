require 'logger'

require 'fiscal_printer/version'
require 'extensions/integer'

require 'fiscal_printer/models/line'
require 'fiscal_printer/models/receipt'
require 'fiscal_printer/models/response'

require 'fiscal_printer/thermal/driver'
require 'fiscal_printer/thermal/message/getprinterinfo'
require 'fiscal_printer/thermal/message/headerget'
require 'fiscal_printer/thermal/message/headerset'
require 'fiscal_printer/thermal/message/lbtrxend'
require 'fiscal_printer/thermal/message/lbtrexitcan'
require 'fiscal_printer/thermal/message/rtcget'
require 'fiscal_printer/thermal/message/trend'
require 'fiscal_printer/thermal/message/trinit'
require 'fiscal_printer/thermal/message/trline'

require 'fiscal_printer/posnet/driver'
require 'fiscal_printer/posnet/message/hdrget'
require 'fiscal_printer/posnet/message/hdrset'
require 'fiscal_printer/posnet/message/papfeed'
require 'fiscal_printer/posnet/message/prncancel'
require 'fiscal_printer/posnet/message/rtcget'
require 'fiscal_printer/posnet/message/trend'
require 'fiscal_printer/posnet/message/trftrend'
require 'fiscal_printer/posnet/message/trftrln'
require 'fiscal_printer/posnet/message/trinit'
require 'fiscal_printer/posnet/message/trline'
require 'fiscal_printer/posnet/message/trpayment'
require 'fiscal_printer/posnet/message/vatget'
require 'fiscal_printer/posnet/message/vatset'

require 'fiscal_printer/serial_client'
require 'fiscal_printer/driver_factory'

module FiscalPrinter
  STX = 0x02.chr.freeze
  ETX = 0x03.chr.freeze
  TAB = 0x09.chr.freeze
  LF  = 0x0A.chr.freeze
  CR  = 0x0D.chr.freeze
  SO  = 0x0E.chr.freeze
  ESC = 0x1B.chr.freeze
  EOS = 0xFF.chr.freeze

  class PrinterError < StandardError
    ConnectionError = 1
    DataError = 2
    NoPaper = 3
    Unknown = 99

    attr_accessor :code

    def initialize(e, code)
      super(e)

      self.code = code
    end
  end

  def self.logger
    logger ||= Logger.new(STDOUT)
  end
end
