require 'validations/custom_predicates'

class BaseSchema < Dry::Validation::Schema
  configure do
    predicates(CustomPredicates)

    config.messages_file = "#{File.dirname(__FILE__)}/../config/errors.yml"
  end
end
