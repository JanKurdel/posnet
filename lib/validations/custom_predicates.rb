require 'dry-validation'

module CustomPredicates
  include Dry::Logic::Predicates

  predicate(:posnet_date?) do |value|
    !%r{^\d{4}[-./]\d{2}[-./]\d{2}$}.match(value).nil?
  end

  predicate(:posnet_datetime?) do |value|
    !%r{^\d{4}[-./]\d{2}[-./]\d{2}[,; ]\d{2}:\d{2}$}.match(value).nil?
  end

  predicate(:posnet_bool?) do |value|
    Dry::Types::Coercions::Posnet::BOOLEAN_MAP.include?(value.to_s)
  end
end
