module FiscalPrinter
  class DriverFactory
    def self.create(protocol, serial)
      case protocol
      when 'thermal'
        Thermal::Driver.new(serial)
      when 'posnet'
        Posnet::Driver.new(serial)
      else
        raise ArgumentError, "Protocol: #{protocol} unknown - not printer driver available"
      end
    end
  end
end
