module FiscalPrinter
  class DriverInterface
    def print_receipt(_receipt)
      raise NotImplementedError, "#{self.class.name} does not implement method"
    end

    # NOTE: current implementation assumes all values are passed
    # {
    #   'A': 23,
    #   'B': 8,
    #   ...
    # }
    def set_ptu(_values)
      raise NotImplementedError, "#{self.class.name} does not implement method"
    end

    def get_ptu
      raise NotImplementedError, "#{self.class.name} does not implement method"
    end
  end
end
