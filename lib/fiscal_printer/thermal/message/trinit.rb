require_relative 'base_message'

# [LBTRSHDR]: Poczatek transakcji
# ESC P Pl $h <check> ESC \
module FiscalPrinter
  module Thermal
    module Message
      class Trinit < BaseMessage
        ID = '$h'.freeze

        Schema = Dry::Validation.Schema(BaseSchema) do
          required(:items_count).filled(:int?, gteq?: 0, lteq?: 80)
        end

        attribute :items_count, Types::Coercible::Int

        def numeric_params
          [items_count]
        end
      end
    end
  end
end
