require_relative 'base_message'
require 'artii'

# [LBTREXIT] : Standardowe zatwierdzenie transakcji
# ESC P Pn; Pc; Py; Px; Pg; Pk; Pz; Pb; Po1; Po2; Pr $x <kod> CR <linia1> CR
# <linia2> CR <linia3> CR <linia4> CR <linia5> CR <nazwa_karty> CR <nazwa_czeku>
# CR <nazwa_bonu> CR TOTAL / RABAT / WPŁATA / KARTA / CZEK/ BON / PRZYJĘCIE/
# WYDANIE / RESZTA / <check> ESC \
module FiscalPrinter
  module Thermal
    module Message
      class Lbtrxend < BaseMessage
        ID = '$x'.freeze

        Schema = Dry::Validation.Schema(BaseSchema) do
          required(:additional_lines).maybe(:array?)
          required(:discount_type).filled(:int?)
          required(:cash_payment_value).maybe(:float?)
          required(:credit_card_payment_value).maybe(:float?)
          required(:check_payment_value).maybe(:float?)
          required(:voucher_payment_value).maybe(:float?)
          required(:total_value).filled(:float?)
          required(:display_change).filled(:int?)
          required(:credit_card_description).filled(:str?)
          required(:check_description).filled(:str?)
          required(:voucher_description).filled(:str?)
          required(:discount).maybe(:float?)
        end

        attribute :additional_lines, Types::Coercible::Array
        attribute :discount_type, Types::Coercible::Int
        attribute :cash_payment_value, Types::Coercible::Float
        attribute :credit_card_payment_value, Types::Coercible::Float
        attribute :check_payment_value, Types::Coercible::Float
        attribute :voucher_payment_value, Types::Coercible::Float
        attribute :total_value, Types::Coercible::Float
        attribute :display_change, Types::Coercible::Int
        attribute :credit_card_description, Types::Coercible::String
        attribute :check_description, Types::Coercible::String
        attribute :voucher_description, Types::Coercible::String
        attribute :discount, Types::Coercible::Float
        
        def numeric_params
          [5, 0, 0, discount_type, cash_payment,
           credit_card_payment, check_payment, voucher_payment, 0, 0, display_change]
        end

        def alphanumeric_params
          "#{CR}#{lines}#{CR}#{credit_card_description}#{CR}#{check_description}" +
            "#{CR}#{voucher_description}#{CR}#{total_value}/#{discount}/" +
            "#{cash_payment_value}/#{credit_card_payment_value}/#{check_payment_value}" +
            "/#{voucher_payment_value}/0.00/0.00/#{cash_payment_value - total_value}/" # check if discount should be applied
        end

        private

        def lines
          #TODO: refactor this
          lines = ((additional_lines || []) + Array.new(5))[0...5]
          lines.join(CR)
        end

        def cash_payment
          cash_payment_value ? 1 : 0
        end

        def credit_card_payment
          credit_card_payment_value ? 1 : 0
        end

        def check_payment
          check_payment_value ? 1 : 0
        end

        def voucher_payment
          voucher_payment_value ? 1 : 0
        end
      end
    end
  end
end
