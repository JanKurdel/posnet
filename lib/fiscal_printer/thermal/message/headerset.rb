require_relative 'base_message'

module FiscalPrinter
  module Thermal
    module Message
      class Headerset < BaseMessage
        ID = '$f'.freeze

        Schema = Dry::Validation.Schema(BaseSchema) do
          required(:header).filled(:str?)
        end

        attribute :header, Types::Coercible::String

        def numeric_params
          [0]
        end

        def alphanumeric_params
          "#{header}#{EOS}"
        end
      end
    end
  end
end
