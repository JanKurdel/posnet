require_relative 'base_message'

module FiscalPrinter
  module Thermal
    module Message
      class Rtcget < BaseMessage
        ID = '#c'.freeze

        def numeric_params
          [0]
        end
      end
    end
  end
end
