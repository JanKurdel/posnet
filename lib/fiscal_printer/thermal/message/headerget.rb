require_relative 'base_message'

module FiscalPrinter
  module Thermal
    module Message
      class Headerget < BaseMessage
        ID = '#u'.freeze

        def numeric_params
          []
        end
      end
    end
  end
end
