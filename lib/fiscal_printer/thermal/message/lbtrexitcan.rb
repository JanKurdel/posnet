require_relative 'base_message'

# [LBTREXITCAN] : Anulowanie transakcji
# ESC P 0 [; Pc ; Pns ] $e [ <nr_kasy> CR <kasjer> CR ] [ <nr systemowy> CR ]
# <check> ESC \
module FiscalPrinter
  module Thermal
    module Message
      class Lbtrexitcan < BaseMessage
        ID = '$e'.freeze

        def numeric_params
          [0]
        end

        def alphanumeric_params
          ''
        end
      end
    end
  end
end
