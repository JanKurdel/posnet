require 'digest/crc16_ccitt'
require 'dry-struct'
require 'dry/core/extensions'
require 'validations/base_schema'

module FiscalPrinter
  module Thermal
    Dry::Types.load_extensions(:maybe)

    module Types
      include Dry::Types.module
    end

    module Message
      class BaseMessage < Dry::Struct
        Schema = Dry::Validation.Schema

        constructor_type :schema

        def to_s
          message
        end

        def valid?
          @valid ||= validation_result.success?
        end

        def errors
          @errors ||= validation_result.errors
        end

        # TODO: need to think about Response model and each message processing
        def process_response(data)
          if data[-2..-1] == "#{ESC}\\"
            response = Response.new(
              error_code: data[2],
              command: self.class.name
            )
          else
            Response.new(
              error_code: -1,
              command: self.class.name
            )
          end
        end

        private

        def validation_result
          @validation_result ||= self.class::Schema.call(to_h)
        end

        def alphanumeric_params
          ''
        end

        def numeric_params
          []
        end

        def message
          "#{ESC}P#{message_payload}#{checksum}#{ESC}\\"
        end

        def message_payload
          @message_payload ||= "#{numeric_params.join(';')}#{self.class::ID}#{alphanumeric_params}"
        end

        def checksum
          @checksum ||= format(
            '%02X', message_payload.bytes.inject(255, :^)
          )
        end
      end
    end
  end
end
