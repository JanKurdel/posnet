require_relative 'base_message'

# [LBFSTRQ]: Odeślij informacje kasowe
# ESC P Ps #s <check> ESC \
module FiscalPrinter
  module Thermal
    module Message
      class Getprinterinfo < BaseMessage
        ID = '#s'.freeze

        Schema = Dry::Validation.Schema(BaseSchema) do
          required(:command).filled(:int?)
        end

        attribute :command, Types::Coercible::Int

        def numeric_params
          [command]
        end

        def alphanumeric_params
          '' 
        end

        def response_class
        end
      end
    end
  end
end
