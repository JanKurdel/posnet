require_relative 'base_message'

# [LBTREXIT] : Standardowe zatwierdzenie transakcji
module FiscalPrinter
  module Thermal
    module Message
      class Trend < BaseMessage
        ID = '$e'.freeze

        Schema = Dry::Validation.Schema(BaseSchema) do
          required(:payment_value).filled(:float?)
          required(:total_value).filled(:float?)
          required(:additional_lines).maybe(:array?)
        end

        attribute :payment_value, Types::Coercible::Float
        attribute :total_value, Types::Coercible::Float
        attribute :additional_lines, Types::Coercible::Array

        def numeric_params
          [1, 0, (additional_lines || []).count, 0]
        end

        # ESC P Pz; Pr $e <kod> CR WPLATA / TOTAL / <check> ESC \
        def alphanumeric_params
          lines = (additional_lines || []).join("#{CR}")
          lines += CR unless lines.empty?
          "1ab#{CR}#{lines}#{payment_value}/#{total_value}/"
        end
      end
    end
  end
end
