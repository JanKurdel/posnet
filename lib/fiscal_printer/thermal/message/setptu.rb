require_relative 'base_message'

# [LBSETPTU] : Zmiana stawek PTU
# ESC P Ps [; Py; Pm; Pd ] $p [ PTU_A / ... PTU_i / ] [<nr_kasy> CR <kasjer> CR] <check> ESC \
#
# NOTE: current implementation requires date to be passed (to avoid new values manual confirmation)
module FiscalPrinter
  module Thermal
    module Message
      class Setptu < BaseMessage
        ID = '$p'.freeze

        Schema = Dry::Validation.Schema(BaseSchema) do
          required(:values).filled { each(:float?, gteq: 0, lteq: 101) }
          required(:year).filled(:int?, gteq?: 0, lteq?: 30)
          required(:month).filled(:int?, gteq?: 1, lteq?: 12)
          required(:day).filled(:int?, gteq?: 1, lteq?: 31)
        end

        attribute :values, Types::Strict::Array.member(Types::Coercible::Float)
        attribute :year, Types::Coercible::Int
        attribute :month, Types::Coercible::Int
        attribute :day, Types::Coercible::Int

        def numeric_params
          [values.count, year, month, day]
        end

        def alphanumeric_params
          "#{values.join('/')}/"
        end
      end
    end
  end
end
