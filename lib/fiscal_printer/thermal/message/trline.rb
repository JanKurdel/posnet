require_relative 'base_message'

# [LBTRSLN] : Linia paragonu
# ESC P Pi $l <nazwa> CR <ilosc> CR <ptu> / CENA / BRUTTO / <check> ESC \
# ESC P Pi ; Pr [;Po] $l <nazwa> CR <ilosc> CR <ptu> / CENA / BRUTTO / RABAT / <check> ESC \
# ESC P Pi ; Pr [;Po] $l <nazwa> CR <ilosc> CR <ptu> / CENA / BRUTTO / RABAT / <OPIS RABATU> CR <check> ESC \
# ESC P Pi ; Pr ; 16 ; 1 $l <nazwa> CR <ilosc> CR <ptu> / CENA / BRUTTO / RABAT / <OPIS RABATU> CR <OPIS TOWARU> CR <check> ESC \
# ESC P Pi ; Pr ; Po ; 1 $l <nazwa> CR <ilosc> CR <ptu> / CENA / BRUTTO / RABAT / <OPIS TOWARU> CR <check> ESC \
# ESC P Pi ; 0 ; 0 ; 1 $l <nazwa> CR <ilosc> CR <ptu> / CENA / BRUTTO / RABAT / <OPIS TOWARU> CR <check> ESC \
module FiscalPrinter
  module Thermal
    module Message
      class Trline < BaseMessage
        ID = '$l'.freeze

        Schema = Dry::Validation.Schema(BaseSchema) do
          required(:line_number).filled(:int?, gteq?: 0, lteq?: 255)
          required(:name).filled(:str?).max_size?(40)
          required(:quantity).filled(:str?) # TODO: implement validation for 1.500 szt. like format
          required(:ptu).filled(:str?, included_in?: ('A'..'G').to_a << 'Z')
          required(:price).filled(:float?, lteq?: 999_999.99)
          required(:total_price).filled(:float?, lteq?: 999_999.99)
          required(:discount_type).maybe(:int?, gteq?: 0, lteq?: 4)
          required(:discount_description).maybe(:int?, gteq?: 0, lteq?: 16)
          required(:discount).maybe(:float?) # TODO: implement validation based on discount_type field
          required(:custom_discount_description).maybe(:str?, max_size?: 20) # TODO: implement validation based on discount_type field
          required(:description).maybe(:str?, max_size?: 35)
        end

        attribute :line_number, Types::Coercible::Int
        attribute :discount_type, Types::Coercible::Int
        attribute :discount_description, Types::Coercible::Int
        attribute :name, Types::Coercible::String
        attribute :quantity, Types::Coercible::String
        attribute :ptu, Types::Coercible::String
        attribute :price, Types::Coercible::Float
        attribute :total_price, Types::Coercible::Float
        attribute :discount, Types::Coercible::Float
        attribute :custom_discount_description, Types::Coercible::String
        attribute :description, Types::Coercible::String

        def numeric_params
          if description
            [line_number, discount_type, discount_description, 1]
          elsif discount_description
            [line_number, discount_type, discount_description]
          elsif discount_type
            [line_number, discount_type]
          else
            [line_number]
          end
        end

        def alphanumeric_params
          params = "#{name}#{CR}#{quantity}#{CR}#{ptu}/#{price}/#{total_price}/"
          params += "#{discount}/" if discount
          if description || custom_discount_description
            "#{params}#{[description, custom_discount_description].compact.join(CR)}#{CR}"
          else
            params
          end
        end
      end
    end
  end
end
