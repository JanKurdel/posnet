require_relative '../driver_interface'

module FiscalPrinter
  module Thermal
    class Driver < DriverInterface
      TIMEOUT_DELAY = 5

      def initialize(serial)
        @serial = serial
      end

      def print_receipt(receipt)
        message = Message::Trinit.new(items_count: receipt.lines.count)
        @serial.write(message) { process_response(message) }
        receipt.lines.each_with_index do |line, idx|
          print_receipt_line(line, idx)
        end

        message = Message::Lbtrxend.new(
          additional_lines: order_number(12),
          cash_payment_value: receipt.payments_total,
          total_value: receipt.price_total
        )
        @serial.write(message) { process_response(message) }
      end

      def set_header(header)
        message = Message::Headerset.new(header: header)
        @serial.write(message) { process_response(message) }
      end

      def get_header
        message = Message::Headerget.new
        @serial.write(message) { process_response(message) }
      end

      def set_ptu(values, year, month, day)
        message = Message::Setptu.new(values: values, year: year, month: month, day: day)
        @serial.write(message) { process_response(message) }
      end

      def get_ptu
        message = Message::Getprinterinfo.new(command: 23)
        @serial.write(message) { process_response(message) }
      end

      private

      # TODO: move this to helper
      def order_number(number)
        a = Artii::Base.new font: 'clr6x6'
        lines = a.asciify("  #{number}  ").split("\n") 
        lines.reject! { |l| l.strip.empty? }  
      end

      def process_response(message)
        FiscalPrinter.logger.debug('Processing response...')

        Timeout::timeout(TIMEOUT_DELAY) do
          loop do
            data = @serial.read(1024)
            unless data.empty?
              FiscalPrinter.logger.debug("Data received: #{data}")
              return message.process_response(data)
            end
            sleep(0.1)
          end
        end
      end

      def print_receipt_line(line, idx)
        message = Message::Trline.new(
          line_number: idx + 1,
          name: line.description,
          quantity: line.count,
          price: line.price,
          total_price: line.total_price,
          ptu: line.ptu
        )
        @serial.write(message) { process_response(message) }
      end
    end
  end
end
