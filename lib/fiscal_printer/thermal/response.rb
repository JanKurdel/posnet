require 'dry-struct'
require 'dry/core/extensions'
require 'validations/base_schema'

module FiscalPrinter
  module Thermal
    module Types
      include Dry::Types.module
    end

    class Response < Dry::Struct
      constructor_type :schema

      Schema = Dry::Validation.Schema(BaseSchema) do
        required(:error_code).filled(:int?)
        required(:command_id).filled(:str?)
      end

      attribute :error_code, Types::Coercible::Int
      attribute :command_id, Types::Coercible::String

      def to_s
        message
      end

      def valid?
        @valid ||= validation_result.success?
      end

      def errors
        @errors ||= validation_result.errors
      end

      private

      def validation_result
        @validation_result ||= self.class::Schema.call(to_h)
      end
    end
  end
end
