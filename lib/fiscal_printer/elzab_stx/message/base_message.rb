require 'digest/crc16_ccitt'
require 'dry-struct'
require 'dry/core/extensions'
require 'validations/base_schema'

module FiscalPrinter
  module ElzabSTX
    module Message
      Dry::Types.load_extensions(:maybe)

      module Types
        include Dry::Types.module
      end

      class BaseMessage < Dry::Struct
        Schema = Dry::Validation.Schema

        constructor_type :schema

        def to_s
          message
        end

        def valid?
          @valid ||= validation_result.success?
        end

        def errors
          @errors ||= validation_result.errors
        end

        private

        def validation_result
          @validation_result ||= self.class::Schema.call(to_h)
        end

        def params
          to_h.map do |k, v|
            "#{TAB}#{k}#{v}" if v
          end.join
        end

        def message
          @message ||= "#{STX}#{message_payload}#{TAB}##{checksum}#{ETX}".force_encoding('BINARY')
        end

        def message_payload
          @message_payload ||= "#{self.class::ID}#{params}"
        end

        def checksum
          @checksum ||= Digest::CRC16CCITT.checksum(message_payload).to_msb_lsb
        end
      end
    end
  end
end
