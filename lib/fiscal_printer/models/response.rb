require_relative 'base_model'

module FiscalPrinter
  class Response < BaseModel
    constructor_type :schema

    attribute :error_code, Types::Coercible::Int
    attribute :error_message, Types::Coercible::Int
    attribute :command, Types::Coercible::String
    attribute :response, Types::Coercible::String
  end
end
