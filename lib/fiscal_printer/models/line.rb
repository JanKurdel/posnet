require_relative 'base_model'

module FiscalPrinter
  LineSchema = Dry::Validation.Schema do
    required(:description).filled(:str?)
    required(:count).filled(:float?)
    required(:price).filled(:float?)
    required(:total_price).filled(:float?)
    required(:ptu).filled(:str?)
  end

  class Line < BaseModel
    attribute :description, Types::Coercible::String
    attribute :count, Types::Coercible::Float
    attribute :price, Types::Coercible::Float
    attribute :total_price, Types::Coercible::Float
    attribute :ptu, Types::Coercible::String
  end
end
