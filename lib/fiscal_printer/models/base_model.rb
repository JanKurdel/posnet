require 'dry-struct'
require 'dry-validation'

module FiscalPrinter
  module Types
    include Dry::Types.module
  end

  class BaseModel < Dry::Struct
    constructor_type :schema
  end
end
