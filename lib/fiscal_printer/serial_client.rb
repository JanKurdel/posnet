require 'rubyserial'

module FiscalPrinter
  class SerialClient
    TIMEOUT_DELAY = 5
    ESC = 0x1B.chr

    def initialize(config = {})
      @serial = serialport = Serial.new(
        config[:port],
        config[:baudrate] || 9600
      )
    rescue RubySerial::Exception => e
      raise PrinterError.new(e, PrinterError::ConnectionError)
    end

    def read(bytes)
      @serial.read(bytes)
    rescue RubySerial::Exception => e
      raise PrinterError.new(e, PrinterError::ConnectionError)
    end

    def write(message)
      FiscalPrinter.logger.debug("Sending message to printer: #{message}")

      begin
        @serial.write(message)
      rescue RubySerial::Exception => e
        raise PrinterError.new(e, PrinterError::ConnectionError)
      end

      yield if block_given?
    end

  end
end
