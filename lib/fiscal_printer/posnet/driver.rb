require_relative '../driver_interface'

module FiscalPrinter
  module Posnet
    class Driver < DriverInterface
      TIMEOUT_DELAY = 5 

      def initialize(serial)
        @serial = serial
      end

      def print_receipt(receipt)
        message = Message::Prncancel.new

        @serial.write(message) { process_response(message) }
        message = Message::Trinit.new(bm: false)
        @serial.write(message) { process_response(message) }
        receipt.lines.each_with_index do |line, idx|
          print_receipt_line(line, idx)
        end

        # 0 - cash
        # 5 - credit
        message = Message::Trpayment.new(ty: 0, wa: receipt.payments_total * 100)
        @serial.write(message) { process_response(message) }
        message = Message::Trend.new(
          to: receipt.price_total * 100,
          fp: receipt.payments_total * 100,
          fe: false
        )
        @serial.write(message) { process_response(message) }

        order_number(12).each do |line|
          message = Message::Trftrln.new(id: 25, na: line)
          @serial.write(message) { process_response(message) }
        end

        message = Message::Trftrend.new
        @serial.write(message) { process_response(message) }
      end

      def set_header(header)
        message = Message::Hdrset.new(tx: formatted_header(header))
        @serial.write(message) { process_response(message) }
      end

      def get_header
        message = Message::Hdrget.new
        @serial.write(message) { process_response(message) }
      end

      def set_ptu(values)
        da = Time.now.strftime("%Y-%m-%d;%H:%M")
        message = Message::Vatset.new(
          da: da,
          va: values[:A],
          vb: values[:B],
          vc: values[:C],
          vd: values[:D],
          ve: values[:E],
          vf: values[:F],
          vg: values[:G],
        )
        @serial.write(message) { process_response(message) }
      end

      def get_ptu
        message = Message::Vatget.new
        @serial.write(message) { process_response(message) }
      end

      private

      def formatted_header(header)
        header
          .gsub('</b>', '').gsub('<b>', '&b')
          .gsub('</center>', '').gsub('<center>', '&c')
      end

      # TODO: move this to helper
      def order_number(number)
        a = Artii::Base.new font: 'clr6x6'
        lines = a.asciify("#{number}  ").split("\n") 
        lines.reject! { |l| l.strip.empty? }  
      end

      def process_response(message)
        FiscalPrinter.logger.debug('Processing response...')

        Timeout::timeout(TIMEOUT_DELAY) do
          loop do
            data = @serial.read(1024)
            unless data.empty?
              FiscalPrinter.logger.debug("Data received: #{data}")
              return message.process_response(data)
            end
            sleep(0.1)
          end
        end
      end

      def print_receipt_line(line, idx)
        message = Message::Trline.new(
          na: line.description,
          vt: 0, #line.ptu.ord - 65,
          il: line.count,
          pr: line.price * 100,
          wa: line.total_price * 100
        )
        @serial.write(message) { process_response(message) }
      end

    end
  end
end
