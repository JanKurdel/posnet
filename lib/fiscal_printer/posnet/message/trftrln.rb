require_relative 'base_message'

module FiscalPrinter
  module Posnet
    module Message
      class Trftrln < BaseMessage
        Schema = Dry::Validation.Schema(BaseSchema) do
          required(:id).filled(:int?)
          required(:na).maybe(:str?)
        end

        ID = 'trftrln'.freeze

        attribute :id, Types::Coercible::Int
        attribute :na, Types::Coercible::String
      end
    end
  end
end
