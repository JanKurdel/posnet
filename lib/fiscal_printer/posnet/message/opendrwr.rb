require 'posnet/base_message'

module Posnet
  module Message
    class Opendrwr < BaseMessage
      ID = 'opendrwr'.freeze
    end
  end
end
