require 'posnet/base_message'

module Posnet
  module Message
    class Formbarcode < BaseMessage
      Schema = Dry::Validation.Schema(BaseSchema) do
        required(:fn).filled(:int?)
        required(:bc).filled(:str?)
      end

      ID = 'formbarcode'.freeze

      attribute :fn, Types::Coercible::Int
      attribute :bc, Types::Coercible::String
    end
  end
end
