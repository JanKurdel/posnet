require_relative 'base_message'

module FiscalPrinter
  module Posnet
    module Message
      class Vatset < BaseMessage
        Schema = Dry::Validation.Schema(BaseSchema) do
          required(:da).maybe(:posnet_datetime?)
        end

        ID = 'vatset'.freeze

        attribute :va, Types::Coercible::Float.optional
        attribute :vb, Types::Coercible::Float.optional
        attribute :vc, Types::Coercible::Float.optional
        attribute :vd, Types::Coercible::Float.optional
        attribute :ve, Types::Coercible::Float.optional
        attribute :vf, Types::Coercible::Float.optional
        attribute :vg, Types::Coercible::Float.optional
        attribute :da, Types::Coercible::String.optional

        def process_response(data)
          response = super(data)

          if response.error_code
            response
          else
            Response.new(
              command: response.command,
              response: vat_rates(data)
            )
          end
        end

        private

        def vat_rates(data)
          result = {}
          data.scan(/\d+,\d+/).each_with_index do |v, i|
            result[(65 + i).chr] = v
          end

          result.to_json
        end
      end
    end
  end
end
