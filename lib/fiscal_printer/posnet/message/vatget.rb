require_relative 'base_message'

require 'json'

module FiscalPrinter
  module Posnet
    module Message
      class Vatget < BaseMessage
        ID = 'vatget'.freeze

        def process_response(data)
          response = super(data)

          if response.error_code
            response
          else
            Response.new(
              command: response.command,
              response: vat_rates(data)
            )
          end
        end

        private

        def vat_rates(data)
          result = {}
          data.scan(/\d+,\d+/).each_with_index do |v, i|
            result[(65 + i).chr] = v
          end

          result.to_json
        end
      end
    end
  end
end
