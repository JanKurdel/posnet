require 'posnet/base_message'

module Posnet
  module Message
    class Formline < BaseMessage
      Schema = Dry::Validation.Schema(BaseSchema) do
        required(:fn).filled(:int?)
        required(:fl).filled(:int?)
      end

      ID = 'formline'.freeze

      attribute :s1, Types::Coercible::String.optional
      attribute :fn, Types::Coercible::Int
      attribute :fl, Types::Coercible::Int
    end
  end
end
