require 'posnet/base_message'

module Posnet
  module Message
    class Formstart < BaseMessage
      Schema = Dry::Validation.Schema(BaseSchema) do
        required(:fn).filled(:int?)
      end

      ID = 'formstart'.freeze

      attribute :fn, Types::Coercible::Int
      attribute :fh, Types::Coercible::Int.optional
    end
  end
end
