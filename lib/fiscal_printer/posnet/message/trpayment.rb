require_relative 'base_message'

module FiscalPrinter
  module Posnet
    module Message
      class Trpayment < BaseMessage
        Schema = Dry::Validation.Schema(BaseSchema) do
          required(:ty).filled(:int?)
          required(:wa).filled(:int?)
          required(:na).maybe(:str?)
          required(:re).maybe(:posnet_bool?)
        end

        ID = 'trpayment'.freeze

        attribute :ty, Types::Coercible::Int
        attribute :wa, Types::Coercible::Int
        attribute :na, Types::Coercible::String
        attribute :re, Types::Posnet::Bool
      end
    end
  end
end
