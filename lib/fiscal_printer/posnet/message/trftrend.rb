require_relative 'base_message'

module FiscalPrinter
  module Posnet
    module Message
      class Trftrend < BaseMessage
        ID = 'trftrend'.freeze
      end
    end
  end
end
