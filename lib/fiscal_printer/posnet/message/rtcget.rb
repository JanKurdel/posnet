require_relative 'base_message'

module FiscalPrinter
  module Posnet
    module Message
      class Rtcget < BaseMessage
        ID = 'rtcget'.freeze
      end
    end
  end
end
