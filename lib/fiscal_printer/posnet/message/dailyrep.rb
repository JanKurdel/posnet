require 'posnet/base_message'

module Posnet
  module Message
    class Dailyrep < BaseMessage
      Schema = Dry::Validation.Schema(BaseSchema) do
        required(:da).maybe(:posnet_date?)
      end

      ID = 'dailyrep'.freeze

      attribute :da, Types::Coercible::String.optional
    end
  end
end
