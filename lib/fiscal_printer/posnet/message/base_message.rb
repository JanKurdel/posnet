require 'digest/crc16_xmodem'

require 'dry-struct'
require 'dry/types/posnet'
require 'dry/core/extensions'
require 'validations/base_schema'

module FiscalPrinter
  module Posnet
    module Message
      Dry::Types.load_extensions(:maybe)

      module Types
        include Dry::Types.module
      end

      class BaseMessage < Dry::Struct
        Schema = Dry::Validation.Schema

        constructor_type :schema

        def to_s
          message
        end

        def valid?
          @valid ||= validation_result.success?
        end

        def errors
          @errors ||= validation_result.errors
        end

        # maybe raise an error when received error
        def process_response(data)
          response = { command: self.class::ID }
          
          result = data.match(/#{STX}ERR#{TAB}.*\?(?<error_code>\d+})#{TAB}#{self.class::ID}/)
          response.merge!({ error_code: result[:error_code] }) if result && result[:error_code]

          result = data.match(/#{STX}#{self.class::ID}#{TAB}\?(?<error_code>\d{4})/)
          response.merge!({ error_code: result[:error_code] }) if result && result[:error_code]

          Response.new(response)
        end

        private

        def validation_result
          @validation_result ||= self.class::Schema.call(to_h)
        end

        def params
          to_h.map do |k, v|
            "#{k}#{v}#{TAB}" if v
          end.join
        end

        def message
          @message ||= "#{STX}#{message_payload}##{checksum}#{ETX}".force_encoding('BINARY')
        end

        def message_payload
          @message_payload ||= "#{self.class::ID}#{TAB}#{params}"
        end

        def checksum
          @checksum ||= Digest::CRC16XModem.hexdigest(message_payload)
        end
      end
    end
  end
end
