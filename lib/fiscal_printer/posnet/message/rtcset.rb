require 'posnet/base_message'

module Posnet
  module Message
    class Rtcset < BaseMessage
      Schema = Dry::Validation.Schema(BaseSchema) do
        required(:da).filled(:posnet_datetime?)
      end

      ID = 'rtcset'.freeze

      attribute :da, Types::Coercible::String
    end
  end
end
