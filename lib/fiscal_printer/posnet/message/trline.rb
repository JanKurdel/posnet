require_relative 'base_message'

module FiscalPrinter
  module Posnet
    module Message
      class Trline < BaseMessage
        Schema = Dry::Validation.Schema(BaseSchema) do
          required(:na).filled(:str?)
          required(:vt).filled(:int?)
          required(:pr).filled(:int?)
          required(:st).maybe(:posnet_bool?)
          required(:wa).maybe(:int?)
          required(:il).maybe(:float?)
          required(:op).maybe(:str?)
          required(:jm).maybe(:str?)
          required(:rd).maybe(:posnet_bool?)
          required(:rn).maybe(:str?)
          required(:rp).maybe(:float?)
          required(:rw).maybe(:int?)
        end

        ID = 'trline'.freeze

        attribute :na, Types::Coercible::String
        attribute :vt, Types::Coercible::Int
        attribute :pr, Types::Coercible::Int
        attribute :st, Types::Posnet::Bool
        attribute :wa, Types::Coercible::Int
        attribute :il, Types::Coercible::Float
        attribute :op, Types::Coercible::String
        attribute :jm, Types::Coercible::String
        attribute :rd, Types::Posnet::Bool
        attribute :rn, Types::Coercible::String
        attribute :rp, Types::Coercible::Float
        attribute :rw, Types::Coercible::Int
      end
    end
  end
end
