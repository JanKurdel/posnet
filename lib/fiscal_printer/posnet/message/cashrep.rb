require 'posnet/base_message'

module FiscalPrinter
  module Posnet
    module Message
      class Cashrep < BaseMessage
        ID = 'cashrep'.freeze
      end
    end
  end
end
