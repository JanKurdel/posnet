require_relative 'base_message'

module FiscalPrinter
  module Posnet
    module Message
      class Trend < BaseMessage
        Schema = Dry::Validation.Schema(BaseSchema) do
          required(:to).maybe(:int?)
          required(:op).maybe(:int?)
          required(:om).maybe(:int?)
          required(:fp).maybe(:int?)
          required(:re).maybe(:int?)
          required(:fe).maybe(:posnet_bool?)
        end

        ID = 'trend'.freeze

        attribute :to, Types::Coercible::Int
        attribute :op, Types::Coercible::Int
        attribute :om, Types::Coercible::Int
        attribute :fp, Types::Coercible::Int
        attribute :re, Types::Coercible::Int
        attribute :fe, Types::Posnet::Bool
      end
    end
  end
end
