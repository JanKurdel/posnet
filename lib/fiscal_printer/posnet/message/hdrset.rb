require_relative 'base_message'

module FiscalPrinter
  module Posnet
    module Message
      class Hdrset < BaseMessage
        Schema = Dry::Validation.Schema(BaseSchema) do
          required(:tx).filled(:str?)
        end

        ID = 'hdrset'.freeze

        attribute :tx, Types::Coercible::String
      end
    end
  end
end
