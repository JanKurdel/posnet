require_relative 'base_message'

module FiscalPrinter
  module Posnet
    module Message
      class Prncancel < BaseMessage
        ID = 'prncancel'.freeze
      end
    end
  end
end
