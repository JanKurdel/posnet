require 'posnet/base_message'

module Posnet
  module Message
    class Formcmd < BaseMessage
      Schema = Dry::Validation.Schema(BaseSchema) do
        required(:fn).filled(:int?)
        required(:cm).filled(:int?)
      end

      ID = 'formcmd'.freeze

      attribute :fn, Types::Coercible::Int
      attribute :cm, Types::Coercible::Int
    end
  end
end
