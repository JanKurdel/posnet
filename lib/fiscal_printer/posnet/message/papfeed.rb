require_relative 'base_message'

module FiscalPrinter
  module Posnet
    module Message
      class Papfeed < BaseMessage
        Schema = Dry::Validation.Schema(BaseSchema) do
          required(:ln).filled(:str?)
        end

        ID = 'papfeed'.freeze

        attribute :ln, Types::Coercible::String
      end
    end
  end
end
