require 'posnet/base_message'

module Posnet
  module Message
    class Formend < BaseMessage
      Schema = Dry::Validation.Schema(BaseSchema) do
        required(:fn).filled(:int?)
      end

      ID = 'formend'.freeze

      attribute :fn, Types::Coercible::Int
    end
  end
end
