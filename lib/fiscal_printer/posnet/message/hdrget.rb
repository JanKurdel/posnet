require_relative 'base_message'

module FiscalPrinter
  module Posnet
    module Message
      class Hdrget < BaseMessage
        ID = 'hdrget'.freeze

        def process_response(data)
          response = super(data)

          if response.error_code
            response
          else
            Response.new(
              command: response.command,
              response: header(data)
            )
          end
        end

        private

        def header(data)
          result = data.match(/\ttx(?<header>.+)\t#/m)
          if result && result[:header]
            result[:header].split("\n").each do |line|
              #TODO: replace here &c and &b with center and b
            end
          end
        end
      end
    end
  end
end
