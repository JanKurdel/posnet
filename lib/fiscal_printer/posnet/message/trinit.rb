require_relative 'base_message'

module FiscalPrinter
  module Posnet
    module Message
      class Trinit < BaseMessage
        Schema = Dry::Validation.Schema(BaseSchema) do
          required(:bm).maybe(:posnet_bool?)
        end

        ID = 'trinit'.freeze

        attribute :bm, Types::Posnet::Bool
      end
    end
  end
end
