module Dry
  module Types
    module Coercions
      module Posnet
        TRUE_VALUES = %w(1 on On ON t true True TRUE T y yes Yes YES Y).freeze
        FALSE_VALUES = %w(0 off Off OFF f false False FALSE F n no No NO N).freeze
        BOOLEAN_MAP = ::Hash[TRUE_VALUES.product([1]) + FALSE_VALUES.product([0])].freeze

        extend Coercions

        # @param [String, Object] input
        # @return [Boolean?]
        # @see TRUE_VALUES
        # @see FALSE_VALUES
        def self.to_true(input)
          BOOLEAN_MAP.fetch(input.to_s, input)
        end

        # @param [String, Object] input
        # @return [Boolean?]
        # @see TRUE_VALUES
        # @see FALSE_VALUES
        def self.to_false(input)
          BOOLEAN_MAP.fetch(input.to_s, input)
        end
      end
    end
  end
end
