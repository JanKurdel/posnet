require 'dry/types/coercions/posnet'

module Dry
  module Types
    register('posnet.true') do
      self['true'].constructor(Coercions::Posnet.method(:to_true))
    end

    register('posnet.false') do
      self['false'].constructor(Coercions::Posnet.method(:to_false))
    end

    register('posnet.bool') do
      (self['posnet.true'] | self['posnet.false']).safe
    end
  end
end
