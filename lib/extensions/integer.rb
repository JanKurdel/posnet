class Integer
  # changes fixnum to 2 bytes msb-lsb
  def to_msb_lsb
    msb = (self / 256).chr
    lsb = (self % 256).chr
    "#{msb}#{lsb}"
  end
end
